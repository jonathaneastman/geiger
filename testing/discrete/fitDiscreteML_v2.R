fitDiscreteML2<-function (phy, data, constraints = NULL, treeTransform = c("none", 
    "lambda", "kappa", "delta", "exponentialChange"), data.names = NULL, qLimits = c(1e-04, 
    1000), pLimits = c(1e-05, 10), root=ROOT.OBS, treePStart=1, qStart=0.1)
{
	treeTransform <- match.arg(treeTransform)
	
	td <- treedata(phy, data, data.names, sort = T)
	
	k<-nlevels(as.factor(td$data))

	baseLik<-diversitree:::make.mkn(td$phy, td$data[,1], k)
	
	nPar<-length(argnames(cLik))	
	if(delta != 1) {
		
		nPar <- nPar + 1
	
		startP<-rep(log(qStart), nPar)
		startP[1]<-log(treePStart)
	
		foo<-function(x) {
			qp<-exp(x[-1])
			dp<-exp(x[1])
			- lnlDiscrete(td$phy, td$data[,1], qpars=qp, delta=dp)
		}

		optim(par=startP, fn=foo, lower=c(-4, -8, -8), upper=c(1, 10, 10), method="L")
	} else if(lambda != 1) { 
				
		nPar <- nPar + 1
	
		startP<-rep(log(qStart), nPar)
		startP[1]<-log(treePStart)
	
		foo<-function(x) {
			qp<-exp(x[-1])
			lp<-exp(x[1])
			- lnlDiscrete(td$phy, td$data[,1], qpars=qp, lambda=lp)
		}

		optim(par=startP, fn=foo, lower=c(-4, -8, -8), upper=c(0, 10, 10), method="L")
	} else if(kappa != 1) {
					
		nPar <- nPar + 1
	
		startP<-rep(log(qStart), nPar)
		startP[1]<-log(treePStart)
	
		foo<-function(x) {
			qp<-exp(x[-1])
			kp<-exp(x[1])
			- lnlDiscrete(td$phy, td$data[,1], qpars=qp, kappa=kp)
		}

		optim(par=startP, fn=foo, lower=c(-4, -8, -8), upper=c(0, 10, 10), method="L")
	
	} else if(exponentialChange != 1) {
				nPar <- nPar + 1
	
		startP<-rep(log(qStart), nPar)
		startP[1]<-log(treePStart)
	
		foo<-function(x) {
			qp<-exp(x[-1])
			ep<-exp(x[1])
			- lnlDiscrete(td$phy, td$data[,1], qpars=qp, exponentialChange =ep)
		}

		optim(par=startP, fn=foo, lower=c(-4, -8, -8), upper=c(0, 10, 10), method="L")
					
	} else {
	
		startP<-rep(log(qStart), nPar)
	
		foo<-function(x) {
			qp<-exp(x)
			- lnlDiscrete(td$phy, td$data[,1], qpars=qp)
		}

		optim(par=startP, fn=foo, lower=c(-8, -8), upper=c(10, 10), method="L")
		#optimx(par=startP, fn=foo, lower=c(-8, -8), upper=c(10, 10), control=list(all.methods=TRUE, save.failures=TRUE, trace=0))


	}
 
	
	
} 


lnlDiscrete<-function (phy, data, qpars, constraints = NULL, delta = 1, lambda = 1, 
    kappa = 1, exponentialChange = 0, f = 1, returnFull = F, root=ROOT.OBS) 
{
    
   	if (class(phy) != "phylo") 
        stop("object \"phy\" is not of class \"phylo\"")

    k<-nlevels(as.factor(tip.data))
        
	likF <- make.mkn.pagel(phy, tip.data, k, delta, lambda, kappa, exponentialChange, control = list(method="exp"))
		
	if(!is.null(constraints)) {
		cLik<-constrain(likF, constraints)
	} else {cLik<-likF}
	
    
    return(cLik(qpars, root)) 
    
}

make.mkn.pagel<-function (tree, states, k, delta=1, lambda=1, kappa=1, exponentialChange=0, strict = TRUE, control = list(method="exp")) 
{
    control <- diversitree:::check.control.mkn(control, k)
    cache <- diversitree:::make.cache.mkn(tree, states, k, strict, control)
    if(delta != 1) {
    	cache<-deltaCache(cache, delta)
    }
    if(lambda != 1) {
    	nTaxa<-length(tree$tip.label)
    	cache<-lambdaCache(cache, lambda, nTaxa)
    }
    if(kappa != 1) {
    	cache<-kappaCache(cache, kappa)
    }
    if(exponentialChange != 0) {
    	cache<-exponentialChangeCache(cache, exponentialChange)
    }
    
    all.branches <- diversitree:::make.all.branches.mkn(cache, control)
    rootfunc <- diversitree:::rootfunc.mkn
    f.pars <- diversitree:::make.pars.mkn(k)
    ll <- function(pars, root = ROOT.OBS, root.p = NULL, intermediates = FALSE) {
        qmat <- f.pars(pars)
        ans <- all.branches(qmat, intermediates)
        rootfunc(ans, qmat, root, root.p, intermediates)
    }
    class(ll) <- c("mkn", "dtlik", "function")
    ll
}

deltaCache<-function(cache, delta) {
		newLen<-(cache$height) ^ delta - (cache$height- cache$len) ^ delta
		cache$len<-newLen
		return(cache)
}

lambdaCache<-function(cache, lambda, nTaxa) {
		newLen<-(cache$len) * lambda
		newLen[1:nTaxa]<-newLen[1:nTaxa]+(cache$height[1:nTaxa]-(cache$height[1:nTaxa]*lambda))
		cache$len<-newLen
		return(cache)
}

kappaCache<-function(cache, kappa) {
		newLen<-(cache$len) ^ kappa
		cache$len<-newLen
		return(cache)
}

exponentialChangeCache<-function(cache, exponentialChange) {
		newLen<-(exp(exponentialChange*(cache$height))-exp(exponentialChange*(cache$height-cache$len)))/exponentialChange
				cache$len<-newLen
		return(cache)
}