#!/bin/sh

## ENSURE THAT these below are the only current dependencies 
curl http://cran.r-project.org/src/contrib/maticce_1.0-3.tar.gz > maticce.tar.gz
R CMD check --as-cran maticce.tar.gz

curl http://cran.r-project.org/src/contrib/PVR_0.2.1.tar.gz > PVR.tar.gz
R CMD check --as-cran PVR.tar.gz

curl http://cran.r-project.org/src/contrib/Reol_1.8.tar.gz > Reol.tar.gz
R CMD check --as-cran Reol.tar.gz
