ACDC.prior <-
function(phy, model, decrease.max = 1.0e-5, increase.max = 1.0e+5) {
	
	if(is.ultrametric(phy)) { 
		max.bt <- max(branching.times(phy))
	} else {
	max.bt <- max(BranchingTimesFossil(phy));
	}
	
	if(model == "ACDC.exp") {
		prior.min <- log(decrease.max) / max.bt;
		prior.max <- log(increase.max) / max.bt;
	} else if(model == "ACDC.lin") {
		prior.min <- (decrease.max - 1) / max.bt;
		prior.max <- (increase.max - 1) / max.bt;
	}
	return(list(min = prior.min, max = prior.max));
}
