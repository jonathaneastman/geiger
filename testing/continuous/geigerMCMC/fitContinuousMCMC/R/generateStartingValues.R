generateStartingValues <-
function(phy, d, C, model, node.priors, root.prior, scaler.prior, sampling.nodes) {
		
		node.pics <- ace(d, phy, type = "continuous", method = "pic")$ace;
		
		if(sampling.nodes == T) {
			node.states <- node.pics[-1];
			if(!is.null(node.priors)){
				repl <- match(node.priors$node, names(node.states))
				for(i in 1:length(repl)) {	
					if(node.priors$prior.type[i] == "normal") node.states[repl[i]] <- rnorm(1, mean = node.priors[i,2], sd = node.priors[i,3]);
					if(node.priors$prior.type[i] == "uniform") node.states[repl[i]] <- runif(1, min = node.priors[i,2], max = node.priors[i,3]);
					if(node.priors$prior.type[i] == "exp") node.states[repl[i]] <- node.priors[i,2] + rexp(1, rate = node.priors[i,3]);
				}			
			}
		d <- c(d, node.states);	
		} else {
			node.states <- NULL;
		}
		
		if(!is.null(root.prior)) {
			if(root.prior$prior.type == "normal") root.val<- rnorm(1, mean = root.prior[1,2], sd =root.prior[1,3]);
			if(root.prior$prior.type == "uniform") root.val <- runif(1, min = root.prior[1,2], max =root.prior[1,3]);
			if(root.prior$prior.type == "exp") root.val <- root.prior[1,2] + rexp(1, rate = root.prior[1,3]);
		} else {
			root.val <- node.pics[1];
		}
		
		if(model == "BM" | model == "Trend") {
			inv.C <- solve(C);
		}
				
		if(model == "BM") { 	
			return(list("sigma" = MLsigmasq(C, d, root.val, inv.C = inv.C)[1,1], "scaler" = NA, "root" = root.val, "node.states" = node.states, "inv.C" = inv.C)); 			
		}else if(model == "OU") {		
				return(list("sigma" = MLsigmasq(C, d, root.val, inv.C = NULL)[1,1], "scaler" = runif(1,10^-5,1), "theta" = root.val, "root" = root.val, "node.states" = node.states)); 
		} else if(model == "SSP") {
				return(list("sigma" = MLsigmasq(C, d, root.val, inv.C = NULL)[1,1], "scaler" = runif(1,10^-5,1), "root" = root.val, "node.states" = node.states)); 	
			} else if(model == "ACDC.exp" | model == "ACDC.lin") {
				return(list("sigma" = MLsigmasq(C, d, root.val, inv.C = NULL)[1,1], "scaler" = 0, "root" = root.val, "node.states" = node.states)); 
			} else if(model == "Trend"){
				return(list("sigma" = MLsigmasq(C, d, root.val, inv.C = inv.C)[1,1], "scaler" = 0, "root" = root.val, "node.states" = node.states,"inv.C" = inv.C)); 
			}
	
	}
