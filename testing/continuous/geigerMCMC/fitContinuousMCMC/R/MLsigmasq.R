MLsigmasq <-
function(C, d, root, inv.C = NULL) {
	
	N <-length(d);
	X <- as.numeric(d);
	EX <- root;
	if(is.null(inv.C)){
		inv.C <- solve(C);
	}
	
	return(((t(X-EX)) %*% inv.C %*% (X - EX)) / N) 
	
	}
