\name{mrca.of.pair}
\alias{mrca.of.pair}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
Find the most recent common ancestor of a pair of taxa.
}
\description{
Given a pair of taxa, this function will return their most recent common ancestor in a phylogeny. If only one tip is given, its parent node is returned.
}
\usage{
mrca.of.pair(phy, tip1, tip2)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{phy}{
A phylogenetic tree in phylo format
}
  \item{tip1}{
the name of a tip in the phylogenetic tree
}
  \item{tip2}{
the name of a tip in the phylogenetic tree. Alternatively, tip2= NULL can be specified and the parent node of tip1 will be returned.
}
}
\details{
}
\value{
a numeric value corresponding to an internal node in the tree phy.
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
Luke J Harmon and Graham J Slater
}
\note{
This function is an internal function of MECCA and fitContinuousMCMC and is made available for general use.
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
data(caniform.data);
## find the node that is the most recent common ancestor of the raccoon and red panda 
mrca.of.pair(caniform.data$phy, tip1 = "Procyon_lotor", tip2 = "Ailurus_fulgens")
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
