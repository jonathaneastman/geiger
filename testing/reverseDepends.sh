#!/bin/sh

## ENSURE THAT these below are the only current dependencies 
curl http://cran.r-project.org/src/contrib/iteRates_3.1.tar.gz > iteRates.tar.gz
R CMD check --as-cran iteRates.tar.gz

curl http://cran.r-project.org/src/contrib/TreeSim_1.8.tar.gz > TreeSim.tar.gz
R CMD check --as-cran TreeSim.tar.gz

curl http://cran.r-project.org/src/contrib/surface_0.3.tar.gz > surface.tar.gz
R CMD check --as-cran surface.tar.gz

